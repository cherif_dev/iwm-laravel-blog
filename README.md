## installation

### Clone the repo
  git clone https://cherif_dev@bitbucket.org/cherif_dev/iwm-laravel-blog.git

###  install dependencies with composer
  composer install

### run db migration and seeding
  php artisan migrate --seed

### run the serve command
  php artisan serve

### Admin account

cherifkabab@gmail.com
password : P@ss+231

### User Account
Look a the table "users" in database
for a generated user
login : Name@gmail.com
password : Name
