<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\JsController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ArticleController::class, 'home'])->name('home');
Route::group([
    'prefix' => 'admin',
    'middleware' => 'auth'
], function () {
    Route::get('/',  [DashboardController::class, 'admin'])->name('admin.dashboard');
    Route::get('/dashboard',  [DashboardController::class, 'index'])->name('admin.dashboard');
});
Route::group(['middleware' => ['auth', 'role:ADMINISTRATEUR'], 'prefix' => 'admin'], function () {

    Route::group([
        'prefix' => 'articles'
    ], function ($router) {
        /**Articles Routes */
        Route::get('/validate', [ArticleController::class, 'admin_validate'])->name('articles.validate');
        Route::post('/validate/{article}/approve', [ArticleController::class, 'approve'])->name('articles.validate.approve');
        Route::post('/validate/{article}/reject', [ArticleController::class, 'reject'])->name('articles.validate.reject');
    });
});

Route::group(['middleware' => ['auth', 'role:UTILISATEUR'], 'prefix' => 'admin'], function () {


    Route::group([], function ($router) {
        /**Articles Routes */
        Route::resource('articles',   ArticleController::class);
    });
});




Route::get('/login', function () {
    return view('auth.login');
})->name('login');

Route::get('/password/reset', function () {
    return view('auth.login');
})->name('password.reset');

Route::name('js.')->group(function () {
    Route::get('dynamic.js',   [JsController::class, 'dynamic'])->name('dynamic');
});

Route::post('/login', [LoginController::class, 'login']);
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
Route::post('/register', [LoginController::class, 'register'])->name('register');
