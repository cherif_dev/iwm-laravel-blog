<?php

namespace Database\Factories;

use App\Models\Article;
use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class ArticleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Article::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $s = ['draft', 'pending', 'approved'];
        $status = Arr::random($s);
        $name = $this->faker->sentence(5);
        $seo_name = strtolower(str_replace(' ', '-', $name));
        return [
            'name' =>   $name,
            'seo_name' =>   $seo_name,
            'description' =>   $this->faker->paragraph(1),
            'content' => $this->faker->paragraph(6),
            'status' =>  $status,
            'image' =>  'https://picsum.photos/700/600',
            'user_id' => User::all()->random()->id,
            'category_id' => Category::all()->random()->id,
        ];
    }
}
