<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserFactory extends Factory
{/**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $gIndex = ($this->faker->numberBetween(0, 1));
        $genders = explode("|", "male|female");
        $gender = $genders[$gIndex];
        $fn = $this->faker->firstName($gender = $gender);
        return [
            'firstname' =>  $fn,
            'lastname' => $this->faker->unique()->lastName(),
            'email_verified_at' => now(),
            "phone" => $this->faker->phoneNumber(),
            "gender" => substr($gender, 0, 1),
            'email' => $fn . '@gmail.com',
            'password' => Hash::make($fn), 
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
