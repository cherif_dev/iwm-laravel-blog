<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = new User([
            "firstname" => "Cherif",
            "lastname" => "KABAB",
            "phone" => "+0215",
            'email_verified_at' => now(),
            "gender" => "m",
            'email' => 'Cherifkabab@gmail.com',
            'password' => Hash::make('P@ss+231'),
        ]);

        $user->save();
        $admin = Role::findByName('ADMINISTRATEUR');

        $user->assignRole($admin);
        $users =  User::factory()
            ->count(9)
            ->create();

        $role = Role::findByName('UTILISATEUR');

        $role->users()->attach($users);
    }
}
