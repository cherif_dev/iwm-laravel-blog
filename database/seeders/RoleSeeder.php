<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder; 
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // Reset cached roles and permissions
         app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();
 
        // create articles Permissions
        Permission::create(['name' => 'articles_create']);
        Permission::create(['name' => 'articles_delete']);
        Permission::create(['name' => 'articles_update']);
        Permission::create(['name' => 'articles_show']);
        Permission::create(['name' => 'articles_comment']);
        Permission::create(['name' => 'articles_approve']);
        Permission::create(['name' => 'articles_reject']);

        $admin = Role::create(['name' => 'ADMINISTRATEUR']);
        $user = Role::create(['name' => 'UTILISATEUR']);
        $visteur = Role::create(['name' => 'VISITEUR']);

        $admin->givePermissionTo(
            [
                'articles_approve',
                'articles_reject',
                'articles_comment',
            ]
        );
        $user->givePermissionTo(
            [
                'articles_create',
                'articles_delete',
                'articles_update',
                'articles_comment',
            ]
        );
        $visteur->givePermissionTo(
            [
                'articles_show',
            ]
        );
    }
}
