<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(['name' => 'Finance']);
        Category::create(['name' => 'Food']);
        Category::create(['name' => 'Business']);
        Category::create(['name' => 'Sports']);
        Category::create(['name' => 'Books']);
        Category::create(['name' => 'Travel']);
        Category::create(['name' => 'Lifestyle']);
        Category::create(['name' => 'Fashion']);
        Category::create(['name' => 'Politics']);
        Category::create(['name' => 'Games']);
        Category::create(['name' => 'Cars']);
    }
}
