<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->string('name');  
            $table->string('seo_name');   
            $table->enum('status', ['draft', 'pending', 'approved','rejected'])->default('draft'); ;
            $table->mediumText('description')->default(''); ;
            $table->longText('content')->default('');  
            $table->string('image')->default(''); 
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('category_id');  
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
