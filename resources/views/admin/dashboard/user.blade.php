@extends('layouts.admin-master')

@section('title')
    Dashboard
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Dashboard</h1>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-info">
                            <i class="far fa-newspaper"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>Total Articles</h4>
                            </div>
                            <div class="card-body">
                                {{ $dashboard['all'] }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-warning">
                            <i class="far  text-white ">
                                <svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="drafting-compass"
                                    role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"
                                    class="svg-inline--fa fa-drafting-compass fa-w-16 fa-fw ">
                                    <path fill="currentColor"
                                        d="M433.86 304.31c21.47-18.17 40.59-39.34 55.75-63.8 2.38-3.83 1.01-8.95-2.89-11.21l-13.86-8.03c-3.73-2.16-8.41-.89-10.69 2.77-12.29 19.75-27.49 37.08-44.51 52.2l-77.71-134.59C347.39 128.03 352 112.63 352 96c0-53.02-42.98-96-96-96s-96 42.98-96 96c0 16.63 4.61 32.03 12.05 45.66L96.11 273.19c-15.81-14.44-30.21-30.57-41.77-49.14-2.28-3.66-6.96-4.94-10.69-2.77l-13.86 8.03c-3.9 2.26-5.27 7.37-2.89 11.21 14.43 23.29 32.76 43.22 53 60.76L0 439.66l7.02 58.25c1.01 8.35 8.11 14.1 15.88 14.1 2.08 0 4.21-.41 6.29-1.3l53.95-23.04 78.85-136.57c30.37 11.71 62.83 18.33 96.26 18.33 32.07 0 63.27-6.12 92.63-16.82l77.98 135.06 53.95 23.04c2.07.88 4.2 1.3 6.29 1.3 7.77 0 14.88-5.75 15.88-14.1l7.02-58.25-78.14-135.35zm-41.75-8.32c-8.87 5.97-18.11 11.3-27.69 16.04l-73.26-126.89c10.11-3.99 19.19-9.78 27.28-16.76l73.67 127.61zM256 32c35.35 0 64 28.65 64 64s-28.65 64-64 64-64-28.65-64-64 28.65-64 64-64zM60.76 462.42l-24.52 10.47-3.19-26.47 160.52-278.04c8.09 6.98 17.17 12.77 27.28 16.76L60.76 462.42zm197.49-125.01c-27.73 0-54.72-5.11-80.11-14.3l75.81-131.31c.7.02 1.34.21 2.04.21s1.35-.19 2.04-.21l76.54 132.57c-24.33 8.22-49.96 13.04-76.32 13.04zm217.51 135.48l-24.52-10.47-70.82-122.66c9.54-4.82 18.74-10.2 27.66-16.1l70.87 122.76-3.19 26.47z"
                                        class=""></path>
                                </svg>
                            </i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>Brouillon</h4>
                            </div>
                            <div class="card-body">
                                {{ $dashboard['drafts'] }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-primary">
                            <i class="fas fa-circle"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>en attente</h4>
                            </div>
                            <div class="card-body">
                                {{ $dashboard['pendings'] }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-success">
                            <i class="far   text-white">
                                <svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="badge-check"
                                    role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"
                                    class="svg-inline--fa fa-drafting-compass fa-w-16 fa-fw ">
                                    <path fill="currentColor"
                                        d="M345.34 182.46a7.98 7.98 0 0 0-5.66-2.34c-2.05 0-4.1.78-5.66 2.34L226.54 289.94l-48.57-48.57a7.98 7.98 0 0 0-5.66-2.34c-2.05 0-4.1.78-5.66 2.34l-11.31 11.31c-3.12 3.12-3.12 8.19 0 11.31l65.54 65.54c1.56 1.56 3.61 2.34 5.66 2.34s4.09-.78 5.65-2.34l124.45-124.45c3.12-3.12 3.12-8.19 0-11.31l-11.3-11.31zM512 256c0-35.5-19.4-68.2-49.6-85.5 9.1-33.6-.3-70.4-25.4-95.5s-61.9-34.5-95.5-25.4C324.2 19.4 291.5 0 256 0s-68.2 19.4-85.5 49.6c-33.6-9.1-70.4.3-95.5 25.4s-34.5 61.9-25.4 95.5C19.4 187.8 0 220.5 0 256s19.4 68.2 49.6 85.5c-9.1 33.6.3 70.4 25.4 95.5 26.5 26.5 63.4 34.1 95.5 25.4 17.4 30.2 50 49.6 85.5 49.6s68.1-19.4 85.5-49.6c32.7 8.9 69.4.7 95.5-25.4 25.1-25.1 34.5-61.9 25.4-95.5 30.2-17.3 49.6-50 49.6-85.5zm-91.1 68.3c5.3 11.8 29.5 54.1-6.5 90.1-28.9 28.9-57.5 21.3-90.1 6.5C319.7 433 307 480 256 480c-52.1 0-64.7-49.5-68.3-59.1-32.6 14.8-61.3 22.2-90.1-6.5-36.8-36.7-10.9-80.5-6.5-90.1C79 319.7 32 307 32 256c0-52.1 49.5-64.7 59.1-68.3-5.3-11.8-29.5-54.1 6.5-90.1 36.8-36.9 80.8-10.7 90.1-6.5C192.3 79 205 32 256 32c52.1 0 64.7 49.5 68.3 59.1 11.8-5.3 54.1-29.5 90.1 6.5 36.8 36.7 10.9 80.5 6.5 90.1C433 192.3 480 205 480 256c0 52.1-49.5 64.7-59.1 68.3z"
                                        class=""></path>
                                </svg>
                            </i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>Approuvés</h4>
                            </div>
                            <div class="card-body">
                                {{ $dashboard['approveds'] }}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
