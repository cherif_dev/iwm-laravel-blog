@csrf
<div class="form-group row mb-4">
    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Title</label>
    <div class="col-sm-12 col-md-7">
        <input type="text" class="form-control" name="name" value="{{ $article->name }}">
    </div>
</div>
<div class="form-group row mb-4">
    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Catégorie</label>
    <div class="col-sm-12 col-md-7">
        <select class="form-control selectric" name="category_id">
            @foreach ($categories as $category)
                <option @if ($category->id == $article->category_id) selected="selected" @endif value="{{ $category->id }}">
                    {{ $category->name }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row mb-4">
    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Contenu</label>
    <div class="col-sm-12 col-md-7">
        <textarea class="summernote-simple" name="content">{{ $article->content }}</textarea>
    </div>
</div>
<div class="form-group row mb-4">
    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Thumbnail</label>
    <div class="col-sm-12 col-md-7">
        <div id="image-preview" class="image-preview" style="background-image: url('/{{ $article->image }}')">
            <label for="image-upload" id="image-label">Choose File</label>
            <input type="file" name="image" id="image-upload" />
        </div>
    </div>
</div>
<div class="form-group row mb-4">
    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tags</label>
    <div class="col-sm-12 col-md-7">
        <input type="text" class="form-control inputtags">
    </div>
</div>


<div class="form-group row mb-4">
    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
    <div class="col-sm-12 col-md-7 d-flex justify-content-between">
        @if ($action == 'create')
            <button type="submit" class="btn btn-danger" name="status" value="draft">Enregistrer</button>
            <button type="submit" class="btn btn-primary " name="status" value="pending">
                Enregistrer & Publier
            </button>
        @endif
        @if ($action == 'edit')
            <button type="submit" class="btn btn-danger" name="status"
                value="{{ $article->status }}">Enregistrer</button>
            @if ($article->status == 'draft')
                <button type="submit" class="btn btn-primary " name="status" value="pending">
                    Enregistrer & Publier
                </button>
            @endif
        @endif

    </div>
</div>
