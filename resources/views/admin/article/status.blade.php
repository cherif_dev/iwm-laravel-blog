@if ($status == 'draft')
    <span class="badge badge-warning">Brouillon</span>
@endif
@if ($status == 'pending')
    <span class="badge badge-primary">En attente de validation</span>
@endif
@if ($status == 'approved')
    <span class="badge badge-success">Approuvé par l'admin</span>
@endif
@if ($status == 'rejected')
    <span class="badge badge-danger">Refusé par l'admin</span>
@endif
