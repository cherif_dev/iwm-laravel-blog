@extends('layouts.admin-master')

@section('title')
    Validation des Articles
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Validation des Articles</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="{{ route('articles.validate') }}">Articles</a></div>
                <div class="breadcrumb-item">Validation des Articles</div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title">Posts</h2>
            <p class="section-lead">
                Vous pouvez gérer toutes les publications, telles que l'édition, la suppression et plus encore.
            </p>

            <div class="row">
                <div class="col-12">
                    <div class="card mb-0">
                        <div class="card-body">

                            <ul class="nav nav-pills">
                                <li class="nav-item">
                                    <a class="nav-link @if (!isset($status)) active @endif" href="{{ route('articles.validate') }}">Tous <span
                                            class="badge badge-white">{{ $dashboard['all'] }}</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link @if ($status=='pending' ) active @endif"
                                        href="{{ route('articles.validate') }}?status=pending">En attente de validation <span
                                            class="badge badge-primary">{{ $dashboard['pendings'] }}</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link @if ($status=='approved' ) active @endif"
                                        href="{{ route('articles.validate') }}?status=approved">Approuvé(s) <span
                                            class="badge badge-success">{{ $dashboard['approveds'] }}</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link @if ($status=='rejected' ) active @endif"
                                        href="{{ route('articles.validate') }}?status=rejected">Annulé(s) <span
                                            class="badge badge-danger">{{ $dashboard['rejecteds'] }}</span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>All Posts</h4>
                        </div>
                        <div class="card-body">
                            <div class="float-left">
                                <select class="form-control selectric">
                                    <option>Action pour la sélection</option>
                                    <option>approver</option>
                                    <option>Annuler</option>
                                </select>
                            </div>
                            <div class="float-right">
                                <form>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="clearfix mb-3"></div>

                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <tr>
                                        <th class="text-center pt-2">
                                            <div class="custom-checkbox custom-checkbox-table custom-control">
                                                <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad"
                                                    class="custom-control-input" id="checkbox-all">
                                                <label for="checkbox-all" class="custom-control-label">&nbsp;</label>
                                            </div>
                                        </th>
                                        <th>Titre</th>
                                        <th>Catégorie</th>
                                        <th>Auteur</th>
                                        <th>Créé à</th>
                                        <th>Statut</th>
                                        <th>Action</th>
                                    </tr>
                                    @foreach ($articles as $article)

                                        <tr>
                                            <td>
                                                <div class="custom-checkbox custom-control">
                                                    <input type="checkbox" data-checkboxes="mygroup"
                                                        class="custom-control-input" id="checkbox-2">
                                                    <label for="checkbox-2" class="custom-control-label">&nbsp;</label>
                                                </div>
                                            </td>


                                            <td>{{ $article->name }}
                                                <div class="table-links">
                                                    <a href="#">Afficher</a>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="#">{{ $article->category->name }}</a>
                                            </td>
                                            <td>
                                                <a href="#">
                                                    <img alt="image" src="{{ asset('assets/img/avatar/avatar-5.png') }}"
                                                        class="rounded-circle" width="35" data-toggle="title" title="">
                                                    <div class="d-inline-block ml-1">
                                                        {{ $article->user->firstname }} {{ $article->user->lastname }}
                                                    </div>
                                                </a>
                                            </td>
                                            <td> {{ $article->created_at->format('d/m/Y') }}</td>
                                            <td>
                                                @include('admin.article.status',['status'=>$article->status])
                                            </td>
                                            <td> 
                                                @if ($article->status == 'pending')
                                                    <button type="button"
                                                        class="btn-approve btn-approve-{{ $article->id }} btn btn-icon icon-left btn-success"
                                                        data-id="{{ $article->id }}"
                                                        data-action="{{ route('articles.validate.approve', ['article' => $article->id]) }}"><i
                                                            class="fas fa-check"></i> approver</button>
                                                    <button type="button"
                                                        class="btn-cancel btn-cancel-{{ $article->id }}  btn btn-icon icon-left btn-danger"
                                                        data-id="{{ $article->id }}"
                                                        data-action="{{ route('articles.validate.reject', ['article' => $article->id]) }}"><i
                                                            class="fas fa-times "></i> Annuler</button>
                                                @endif
                                                @if ($article->status == 'approved')
                                                    <button type="button"
                                                        class="btn-cancel btn-cancel-{{ $article->id }}  btn btn-icon icon-left btn-danger"
                                                        data-id="{{ $article->id }}"
                                                        data-action="{{ route('articles.validate.reject', ['article' => $article->id]) }}"><i
                                                            class="fas fa-times "></i> Annuler</button>
                                                @endif

                                            </td>
                                        </tr>
                                    @endforeach

                                </table>
                            </div>
                            <div class="float-right">
                                <nav>
                                    <ul class="pagination">
                                        <li class="page-item disabled">
                                            <a class="page-link" href="#" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                        </li>
                                        <li class="page-item active">
                                            <a class="page-link" href="#">1</a>
                                        </li>
                                        <li class="page-item">
                                            <a class="page-link" href="#">2</a>
                                        </li>
                                        <li class="page-item">
                                            <a class="page-link" href="#">3</a>
                                        </li>
                                        <li class="page-item">
                                            <a class="page-link" href="#" aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('assets/modules/jquery-selectric/jquery.selectric.min.js') }}"></script>
    <script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
    <!-- Page Specific JS File -->
    <script src="{{ asset('assets/js/page/features-posts.js') }}"></script>
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });
            $('.btn-approve').on('click', function() {
                let $this = $(this);
                let action = $this.data('action');
                call(action, function(data) {
                    console.log(data)
                    swal('Article approuvé', "Vous avez  approuvé l'article : " + data.name,
                        'success');
                    $this.hide().remove();
                })
            })
            $('.btn-cancel').on('click', function() {
                let $this = $(this);
                let action = $this.data('action');
                call(action, function(data) {
                    swal('Article annulé', "Vous avez  annulé l'article : " + data.name,
                        'success');
                })

            })

            function call(action, cb) {
                $.post(action, function(data) {
                    cb(data)
                });
            }
        })
    </script>
@endsection
