@extends('layouts.admin-master')

@section('title')
    Edition de l'article {{ $article->name }}
@endsection

@section('content')
    <style>
        .image-preview,
        #callback-preview {
            background-size: cover;
            background-position: center center;
        }

    </style>
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="features-posts.html" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
            </div>
            <h1>Edition de l'article #{{ $article->id }}</h1>

            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="{{ route('articles.index') }}">Articles</a></div>
                <div class="breadcrumb-item">#{{ $article->id }}</div>
            </div>
        </div>

        <div class="section-body">
        
            <h2 class="section-title">Edition de l'article {{ $article->name }}</h2>
            <p class="section-lead">
                Sur cette page, vous pouvez editer l'article en changeant les valeurs souahitées.
            </p>
         
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between">
                            <h4>Modifier votre article</h4>
                            @include('admin.article.status',['status'=>$article->status])
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('articles.update', ['article' => $article->id]) }}"
                                enctype="multipart/form-data">
                                @method('PUT')
                                @include('admin.article.form',['action'=>"edit"])
                            </form>
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('scripts')
    <script src="{{ asset('assets/modules/summernote/summernote-bs4.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-selectric/jquery.selectric.min.js') }}"></script>
    <script src="{{ asset('assets/modules/upload-preview/assets/js/jquery.uploadPreview.min.js') }}"></script>
    <script src="{{ asset('assets/modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('assets/js/page/features-post-create.js') }}"></script>
@endsection
