@extends('layouts.blog-master')

@section('content')
    <style>
        .date__box {
            position: absolute;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            color: #ccc;
            border: 4px solid;
            font-weight: bold;
            padding: 5px 10px;
        }

        .date__box .date__day {
            font-size: 22px;
        }

        .blog-card {
            padding: 30px;
            position: relative;
        }

        .blog-card .date__box {
            opacity: 0;
            transform: scale(0.5);
            transition: 300ms ease-in-out;
        }

        .blog-card .blog-card__background,
        .blog-card .card__background--layer {
            z-index: -1;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        .blog-card .blog-card__background {
            padding: 15px;
            background: white;
        }

        .blog-card .card__background--wrapper {
            height: 100%;
            clip-path: polygon(0 0, 100% 0, 100% 80%, 0 60%);
            position: relative;
            overflow: hidden;
        }

        .blog-card .card__background--main {
            height: 100%;
            position: relative;
            transition: 300ms ease-in-out;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .blog-card .card__background--layer {
            z-index: 0;
            opacity: 0;
            background: rgba(51, 51, 51, 0.9);
            transition: 300ms ease-in-out;
        }

        .blog-card .blog-card__head {
            height: 300px;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .blog-card .blog-card__info {
            z-index: 10;
            background: #f7f8ff;
            padding: 20px 15px;
        }

        .blog-card .blog-card__info h5 {
            transition: 300ms ease-in-out;
        }

        .blog-card:hover .date__box {
            opacity: 1;
            transform: scale(1);
        }

        .blog-card:hover .card__background--main {
            transform: scale(1.2) rotate(5deg);
        }

        .blog-card:hover .card__background--layer {
            opacity: 1;
        }

        .blog-card:hover .blog-card__info h5 {
            color: #6777ef;
        }

        a.icon-link {
            color: #363738;
            transition: 200ms ease-in-out;
        }

        a.icon-link i {
            color: #6777ef;
        }

        a.icon-link:hover {
            color: #6777ef;
            text-decoration: none;
        }

        .blg-btn {
            background: white;
            color: #363738;
            font-weight: bold;
            outline: none;
            box-shadow: 1px 1px 3px 0 rgba(0, 0, 0, 0.2);
            overflow: hidden;
            border-radius: 0;
            height: 50px;
            line-height: 50px;
            display: inline-block;
            padding: 0;
            border: none;
        }

        .blg-btn:focus {
            box-shadow: none;
        }

        .blg-btn:hover {
            background: #6777ef;
            color: #fff;
        }

        .blg-btn.blg-btn--with-icon {
            padding-right: 20px;
        }

        .blg-btn.blg-btn--with-icon i {
            padding: 0px 30px 0px 15px;
            margin-right: 10px;
            height: 50px;
            line-height: 50px;
            vertical-align: bottom;
            color: white;
            background: #6777ef;
            clip-path: polygon(0 0, 70% 0, 100% 100%, 0% 100%);
        }

        .blg-btn.blg-btn--only-icon {
            width: 50px;
        }

    </style>
    <div class="row">
        <div class="col-12 col-sm-8 col-lg-9">
            @foreach ($articles as $article)
                <article class="blog-card mb-4 shadow-sm">
                    <div class="blog-card__background">
                        <div class="card__background--wrapper">
                            <div class="card__background--main"
                                style="background-image: url('{{ $article->image }}?hmac={{ rand(2, 50) }}');">
                                <div class="card__background--layer"></div>
                            </div>
                        </div>
                    </div>
                    <div class="blog-card__head">
                        <span class="date__box">
                            <span class="date__day">{{ $article->created_at->format('d') }}</span>
                            <span class="date__month">{{ $article->created_at->format('M') }}</span>
                        </span>
                    </div>
                    <div class="blog-card__info">
                        <div class=" d-flex justify-content-between mb-2 ">
                            <span class="badge badge-info mb-2">{{ $article->category->name }}</span>
                            <button type="button" class="btn btn-primary btn-icon icon-left">
                                <i class="fas fa-comment"></i> Commentaires <span class="badge badge-transparent">150</span>
                            </button>
                        </div>
                        <h5>{{ $article->name }}</h5>


                        <div class="d-flex justify-content-between mb-2 ">
                            <a href="#">
                                <img alt="image" src="{{ asset('assets/img/avatar/avatar-5.png') }}"
                                    class="rounded-circle" width="35" data-toggle="title" title="">
                                <div class="d-inline-block ml-1">
                                    {{ $article->user->firstname }} {{ $article->user->lastname }}
                                </div>
                            </a>

                        </div>
                        <p>{{ $article->description }}</p>
                        <a href="#" class="blg-btn blg-btn--with-icon"><i class="blg-btn-icon fa fa-arrow-right"></i>
                            Lire la suite</a>
                    </div>
                </article>
            @endforeach

        </div>

        <div class="col-12 col-sm-4 col-lg-3">

            <div class="card">
                <div class="card-header">
                    <h4>Catégories</h4>
                </div>
                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        @foreach ($categories as $category)
                            <li class="list-group-item"><a href="#"> {{ $category->name }}</a></li>
                        @endforeach

                    </ul>

                </div>
            </div>
        </div>

    </div>
@endsection
