    {{-- <aside id="sidebar-wrapper">
  <div class="sidebar-brand">
    <a href="{{ route('admin.dashboard') }}">{{ env('APP_NAME') }}</a>
  </div>
  <div class="sidebar-brand sidebar-brand-sm">
    <a href="index.html">St</a>
  </div>
  <ul class="sidebar-menu">
      <li class="menu-header">Dashboard</li>
      <li class="{{ Request::route()->getName() == 'admin.dashboard' ? ' active' : '' }}"><a class="nav-link" href="{{ route('admin.dashboard') }}"><i class="fa fa-columns"></i> <span>Dashboard</span></a></li>
      @if (Auth::user()->hasRole('UTILISATEUR'))
     
   <li class="menu-header">Users</li>
       <li class="{{ Request::route()->getName() == 'admin.users' ? ' active' : '' }}"><a class="nav-link" href="{{ route('admin.users') }}"><i class="fa fa-users"></i> <span>Users</span></a></li>
    
      <li class="dropdown">
        <a href="#" class="nav-link " data-toggle="dropdown"><i class="fas fa-columns"></i> <span>Layout</span></a>
        <ul class="dropdown-menu" style="display: block;">
          <li><a class="nav-link" href="layout-default.html">Default Layout</a></li>
          <li><a class="nav-link" href="layout-transparent.html">Transparent Sidebar</a></li>
          <li><a class="nav-link" href="layout-top-navigation.html">Top Navigation</a></li>
        </ul>
      </li>
      @endif
    </ul>
</aside> --}}
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ route('home') }}">IWM-BLOG</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ route('home') }}">IWM</a>
        </div>
        <ul class="sidebar-menu">

            <li class=" {{ Request::segments()[1] == 'dashboard' ? ' active' : '' }}"><a class="nav-link"
                    href="{{ route('admin.dashboard') }}"><i class="fas fa-fire"></i> <span>Overview</span></a></li>
            @if (Auth::user()->hasRole('UTILISATEUR'))

                <li class="dropdown {{ Request::segments()[1] == 'articles' ? ' active' : '' }}  ">
                    <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-newspaper"></i>
                        <span>Articles</span></a>
                    <ul class="dropdown-menu ">
                        <li><a class="nav-link" href="{{ route('articles.index') }}">Afficher</a></li>
                        <li><a class="nav-link" href="{{ route('articles.create') }}">Créer un article</a></li>
                    </ul>
                </li>
            @endif
            @if (Auth::user()->hasRole('ADMINISTRATEUR'))
                <li class="active"><a class="nav-link" href="{{ route('articles.validate') }}"><i
                            class="fas fa-pencil-ruler"></i> <span>Articles</span></a></li>

            @endif
        </ul>
    </aside>
