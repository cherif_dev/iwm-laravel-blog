<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\ROLES;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    //
    public function index(Request $request)
    {
        /** @var \App\Models\User */
        $user = Auth::user();
        if ($user->hasRole(ROLES::ADMINISTRATEUR))
            return $this->admin();
        if ($user->hasRole(ROLES::UTILISATEUR))
            return $this->user();
    }

    public function  admin()
    { 
        $dashboard = [];
        $dashboard['drafts'] = Article::where('status', 'draft')->count();
        $dashboard['rejecteds'] = Article::where('status', 'rejected')->count();
        $dashboard['pendings'] = Article::where('status', 'pending')->count();
        $dashboard['approveds'] = Article::where('status', 'approved')->count();
        $dashboard['all'] =  $dashboard['rejecteds']  +  $dashboard['pendings'] +    $dashboard['approveds'];
        return view('admin.dashboard.index', [   'dashboard' => $dashboard]);
        return  view('admin.dashboard.index');
    }


    public function  user()
    {
        /** @var \App\Models\User */
        $user = Auth::user(); 
        $dashboard = [];
        $dashboard['drafts'] = $user->articles()->where('status', 'draft')->count();
        $dashboard['pendings'] = $user->articles()->where('status', 'pending')->count();
        $dashboard['approveds'] = $user->articles()->where('status', 'approved')->count();
        $dashboard['all'] = $user->articles()->count();
        return view('admin.dashboard.user', ['dashboard' => $dashboard]);
    }
}
