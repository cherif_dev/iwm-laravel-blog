<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Validator;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\Console\Input\Input;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function home(Request $request)
    {
        $categories = Category::all();
        $articles = Article::where('status', 'approved')->get(); 
        return view('welcome', ['articles' => $articles, 'categories' =>  $categories]);
    }

    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /** @var \App\Models\User */
        $user = Auth::user();

        $status = $request->status;
        if (isset($status))
            $articles = $user->articles()->where('status', $status)->get();
        else  $articles = $user->articles()->get();
        $dashboard = [];
        $dashboard['drafts'] = $user->articles()->where('status', 'draft')->count();
        $dashboard['pendings'] = $user->articles()->where('status', 'pending')->count();
        $dashboard['approveds'] = $user->articles()->where('status', 'approved')->count();
        $dashboard['all'] = $user->articles()->count();
        return view('admin.article.index', ['articles' => $articles, 'status' => $status, 'dashboard' => $dashboard]);
    }

    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function admin_validate(Request $request)
    {
        /** @var \App\Models\User */
        $user = Auth::user();

        $status = $request->status;
        if (isset($status))
            $articles = Article::where('status', $status)->get();
        else  $articles = Article::whereIn('status', ['rejected', 'pending', 'approved'])->get();
        $dashboard = [];
        $dashboard['rejecteds'] = Article::where('status', 'rejected')->count();
        $dashboard['pendings'] = Article::where('status', 'pending')->count();
        $dashboard['approveds'] = Article::where('status', 'approved')->count();
        $dashboard['all'] =  $dashboard['rejecteds']  +  $dashboard['pendings'] +    $dashboard['approveds'];
        return view('admin.article.validate', ['articles' => $articles, 'status' => $status, 'dashboard' => $dashboard]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.article.create', ['categories' => $categories, "article" => new Article()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => "required|string",
            'category_id' => "required|integer",
            'image' => 'required|mimes:jpeg,jpg,png,gif|required|max:10000',
            'status' => "required|string"
        ]);

        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        $inputs = $request->all();
        $article = new Article($inputs);
        $article->user_id = Auth::user()->id;
        $article->seo_name = "";
        $imageName = time() . '.' . $request->image->extension();

        $request->image->move(public_path('assets/img/articles'), $imageName);

        $article->image = 'assets/img/articles/' . $imageName;
        $article->save();
        return redirect()->route('articles.edit', ['article' =>  $article->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article =  Article::find($id);
        $categories = Category::all();
        return view('admin.article.edit', ['categories' => $categories, "article" => $article]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => "string",
            'category_id' => "integer",
            'status' => "string"
        ]);

        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        $inputs = $request->all();
        $article =   Article::find($id);
        $article->user_id = Auth::user()->id;
        $article->seo_name = "";
        if ($request->image) {
            $imageName = time() . '.' . $request->image->extension();

            $request->image->move(public_path('assets/img/articles'), $imageName);
            //TODO : Remove old image after upload

            $article->image = 'assets/img/articles/' . $imageName;
        }

        $article->update($inputs);
        return redirect()->route('articles.edit', ['article' =>  $article->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * approve the article by admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        $article =  Article::find($id);
        $article->status = "approved";
        $article->save();
        return response()->json($article, 200);
    }


    /**
     * reject the article by admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reject($id)
    {
        $article =  Article::find($id);
        $article->status = "rejected";
        $article->save();
        return response()->json($article, 200);
    }
}
