<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;

use Closure;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role = null)
    {
        $user = Auth::user();
        $is_login = $request->is('login');
        $is_logout = $request->is('logout');
        $is_post = ($request->isMethod('post'));

        //deny access if logout and method is get 
        if ($is_logout  && !$is_post) {
            abort(403);
        }

        if (!$is_login) {
            //deny access if user has no role
            if ($user &&  $user->roles()->count() == 0) {
                abort(403, "vous n'avez pas le bon rôle pour accéder à cette page");
            }
            //deny access  if the user has a role that does not match the acceptable roles
            if ($role != null) {
                $roles = null;
                if (str_contains($role, '|')) {
                    $roles = explode("|", $role);
                } else {
                    $roles[] = $role;
                } 
                if ($user && !$user->hasAnyRole($roles)) {
                    abort(403, "vous n'avez pas le bon rôle pour accéder à cette page");
                }
            }
        }

        return $next($request);
    }
}
